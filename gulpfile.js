var gulp = require('gulp'),
    browserSync = require('browser-sync').create(),// 浏览器同步
    gulpIf = require('gulp-if'),
    sass = require('gulp-sass'),
    autoprefixer = require('gulp-autoprefixer'),//可以解析CSS文件，并且添加浏览器前缀到CSS规则里,
    concat = require('gulp-concat'),//js合併
    minifyCss = require('gulp-minify-css'),//css壓縮
    uglify = require('gulp-uglify'),//js壓縮
    rename = require('gulp-rename'),// 重命名文件
    sourcemaps = require('gulp-sourcemaps'),// sass地图
    plumber = require('gulp-plumber'),//報錯不中斷當前任務
    gutil = require('gulp-util'),//打印
    iconfont = require('gulp-iconfont'),
    connect = require('gulp-connect-php'),
    env = require('gulp-env'),
    iconfont = require('gulp-iconfont'),
    consolidate = require("gulp-consolidate"),
    spritesmith = require('gulp.spritesmith'),
    merge = require('merge-stream'), //成環狀的東西向一些流添加到合併流
    processhtml = require('gulp-processhtml'),//處理html文件單層
    header = require('gulp-header'),
    fs = require('fs');//到自己設定的路線
var prettify = require('gulp-jsbeautifier'),//css美化
    extender    = require('gulp-html-extend'),//處理html文件無限層
    gulpImagemin = require('gulp-imagemin');//圖壓縮
var saveLicense = require('uglify-save-license'); //保留宣告
//var bom = require('gulp-bom');
//


var reload = browserSync.reload;
var pkg = JSON.parse(fs.readFileSync('./package.json'));
var banner = '/*! <%= new Date().toDateString() %> */\n';
var banners = '/*! <%= new Date().toDateString() %> | <%= pkg.author %> */\n';

//environment variables
env({
    file: ".env.json"
});


var source_src = 'marco_polo/';
// var source_src = 'seaport_artcenter/';

var src = source_src + 'source',
    dest = source_src + 'public';

var env = {
    isProduction: false,
    autoprefixer: ['> 0%']
};

var pluginCSS = {
     isProduction: true,
},
    mainCSS = {
     isProduction: true,
},
    pluginJS = {
     isProduction: false,
},
    mainJS = {
     isProduction: false,
};


//serve php files
// gulp.task('php', function() {
//     connect.server({
//         base: dest,
//         port: process.env.PHPPORT
//     });
// });

gulp.task('serve', function() {
    browserSync.init({
        // proxy: process.env.PROXY,
        port: process.env.PORT,
        server: {
            baseDir: dest
        },
        notify: false,
        open: true,
    });

    gulp.watch(dest + '/images/icons/*.png', ['make:sprite']);
    gulp.watch(src + '/sass/**/*.scss', ['make:css']);
    gulp.watch(src + '/js/**/*js', ['make:app-js', 'make:js', 'make:plugin-js', 'make:ie8-js']);
    gulp.watch(src + '/js/**/*.js').on('change', reload);
    gulp.watch(src + '/src/*.html', ['make:html']);
    gulp.watch(src + '/src/component/*.html', ['make:include-html']);
//    gulp.watch([src + '/src/component/*.html', ], ['make:include-html']);
});

gulp.task('make:html', function(){
    var opts = { /* plugin options */ };
    return gulp.src(src + '/src/**/*.html')
               .pipe(processhtml(opts))
               .pipe(extender({annotations:false,verbose:false})) // default options 
               .pipe(gulp.dest(dest + '/'))
               .pipe(browserSync.stream());
});

gulp.task('make:include-html', function(){
    var opts = { /* plugin options */ };
    return gulp.src(src + '/src/*.html')
               .pipe(processhtml(opts))
               .pipe(extender({annotations:false,verbose:false})) // default options 
               .pipe(gulp.dest(dest + '/'))
               .pipe(browserSync.stream());
});

gulp.task('make:css', function() {
    return gulp.src(src + '/sass/**/*.scss')
        .pipe(plumber(function(error) {
            gutil.beep();
            gutil.log(gutil.colors.red(error.message));
            this.emit('end');
        }))
        .pipe(gulpIf(!env.isProduction, sourcemaps.init()))//scss路徑 關閉可關掉輸出後方亂碼
        .pipe(sass())
        .pipe(autoprefixer(env.autoprefixer))
        .pipe(gulpIf(!env.isProduction, sourcemaps.write()))//scss路徑 關閉可關掉輸出後方亂碼
        .pipe(header(banners, {pkg: pkg}))
        .pipe(prettify())//美化
        .pipe(gulpIf(mainCSS.isProduction, minifyCss()))
        .pipe(gulp.dest(dest + '/css'))
        .pipe(browserSync.stream());
});

gulp.task('make:plugin-css', function() {
    return gulp.src([
            src + '/css/plugin/**.css'
        ])
        .pipe(concat('plugin.css'))
        .pipe(gulpIf(!env.isProduction, sourcemaps.init()))
        .pipe(gulpIf(!env.isProduction, sourcemaps.write()))
        .pipe(gulpIf(pluginCSS.isProduction, minifyCss()))
        .pipe(header(banner, {pkg: pkg}))
        .pipe(gulp.dest(dest + '/css'))
        .pipe(browserSync.stream());
});

gulp.task('make:ie8-js', function() {
    return gulp.src([
            // src + '/js/plugin/respond.min.js',
            // src + '/js/plugin/html5shiv.js'

        ])
        .pipe(concat('ie8-fix.min.js'))
        .pipe(gulpIf(pluginJS.isProduction, uglify()))
        .pipe(header(banner, {pkg: pkg}))
        .pipe(gulp.dest(dest + '/js'));
});

gulp.task('make:plugin-js', function() {
    // console.log(src+'/js/plugin/jquery.js');
    return gulp.src([
            src+'/js/plugin/jquery/jquery-2.2.0.min.js',
            src + '/js/plugin/**.js'
            // src + '/js/plugin/slick.js',
            // src + '/js/plugin/pace.min.js',

        ])
        .pipe(concat('plugin.js'))
        .pipe(gulpIf(pluginJS.isProduction, uglify({
                    output: {
                        comments: saveLicense
                    }
                })))
        .pipe(header(banner, {pkg: pkg}))
        .pipe(gulp.dest(dest + '/js'));
});

gulp.task('make:app-js', function() {
    return gulp.src([
            // src + '/js/app.js',
            // src + '/js/controllers/*.js',
            // src + '/js/directives/*.js'
        ])
        .pipe(concat('all-app.js'))
        .pipe(header(banner, {pkg: pkg}))
        .pipe(gulp.dest(dest + '/js'));
});

gulp.task('make:js', function() {
    return gulp.src([
            src + '/js/main.js'
        ])
        .pipe(concat('main.js'))
        .pipe(gulpIf(mainJS.isProduction, uglify()))
        .pipe(header(banners, {pkg: pkg}))
        .pipe(gulp.dest(dest + '/js'));
})

gulp.task('make:iconfont', function() {
    var fontName = 'icons';
    var template = 'icons-tpl';
    return gulp.src(['source/icons/*.svg'])
        .pipe(iconfont({
            fontName: fontName,
            normalize: true,
            fontHeight: 1001,
            formats: ['ttf', 'eot', 'woff']
        }))
        .on('glyphs', function(glyphs) {
            var options = {
                glyphs: glyphs.map(function(glyph) {
                    return {
                        name: glyph.name,
                        codepoint: glyph.unicode[0].charCodeAt(0)
                    }
                }),
                fontName: fontName,
                fontPath: '../fonts/',
                className: 'icons'
            };

            gulp.src(src + '/icons/template/' + template + '.css')
                .pipe(consolidate('lodash', options))
                .pipe(rename({ basename: fontName }))
                .pipe(header(banner, {pkg: pkg}))
                .pipe(gulp.dest(dest + '/css/'));

            gulp.src(src + '/icons/template/' + template + '.html')
                .pipe(consolidate('lodash', options))
                .pipe(rename({ basename: 'sample' }))
                .pipe(gulp.dest(dest + '/css/'));
        })
        .pipe(gulp.dest(dest + '/fonts/'));
});

gulp.task('make:sprite', function() {
    // console.log(dest + 'images/icons/*.png');
    var spriteData = gulp.src(dest + '/images/icons/*.png').pipe(spritesmith({
        imgName: 'sprite.png',
        cssName: '_sprites-icons.scss',
        cssFormat: 'scss',
        imgPath: '../images/sprite.png'
    }));

    var imgStream = spriteData.img.pipe(gulp.dest(dest + '/images/'));
    var cssStream = spriteData.css.pipe(gulp.dest(src + '/sass/sprites/'));
    return merge(imgStream, cssStream);
    // return imgStream;
});
//gulp.task('image', function () {
//    gulp.src( src+ '/images/**')
//        .pipe(gulpImagemin())
//        .pipe(gulp.dest(dest+'/images'));
//});
//run gulp

  
gulp.task('default', ['make:sprite', 'make:css', 'make:plugin-css', 'make:app-js', 'make:js', 'make:plugin-js', 'make:ie8-js', 'make:iconfont','serve']);