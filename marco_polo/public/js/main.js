/*! Thu Aug 30 2018 | Jasmine Yeh */
$(function() {
    // reload Page scroll to top
    window.onbeforeunload = function() {
        window.scrollTo(0, 0);
    }
    $("html, body").animate({ scrollTop: 0 }, 500);

    // 店家的輪播
    $('.store1_slider').slick({
        dots: true,
        infinite: true,
        autoplay: true,
        autoplaySpeed: 3000
    });
    $('.store2_slider').slick({
        dots: true,
        infinite: true,
        autoplay: true,
        autoplaySpeed: 3000
    });
    $('.store3_slider').slick({
        dots: true,
        infinite: true,
        autoplay: true,
        autoplaySpeed: 3000
    });

    
    
    // 舊版首頁的印象 馬哥
    // $().timelinr({
    //     containerDiv: '#timeline',
    //     // value: any HTML tag or# id,
    //     // default to# timeline
    //     datesDiv: '#dates',
    //     // value: any HTML tag or #id, default to #dates
    //     datesSelectedClass: 'selected',
    //     // value: any class, default to selected
    //     datesSpeed: 'normal',
    //     // value: integer between 100 and 1000 (recommended) or 'slow', 'normal' or 'fast'; default to normal
    //     issuesDiv: '#issues',
    //     // value: any HTML tag or #id, default to #issues
    //     issuesSelectedClass: 'selected',
    //     // value: any class, default to selected
    //     issuesSpeed: 'fast',
    //     // value: integer between 100 and 1000 (recommended) or 'slow', 'normal' or 'fast'; default to fast
    //     issuesTransparency: 0.2,
    //     // value: integer between 0 and 1 (recommended), default to 0.2
    //     issuesTransparencySpeed: 500,
    //     // value: integer between 100 and 1000 (recommended), default to 500 (normal)
    //     prevButton: '#prev',
    //     // value: any HTML tag or #id, default to #prev
    //     nextButton: '#next',
    //     // value: any HTML tag or #id, default to #next
    //     arrowKeys: 'false',
    //     // value: true/false, default to false
    //     startAt: 1,
    //     // value: integer, default to 1 (first)
    //     autoPlay: 'false',
    //     // value: true | false, default to false
    //     autoPlayDirection: 'forward',
    //     // value: forward | backward, default to forward
    //     autoPlayPause: 2000
    //     // value: integer (1000 = 1 seg), default to 2000 (2segs)<
    // });

    // readTextFile("./file/banner/banner_text.txt");

    // index page
    $.getJSON("./file/data.json", function (data) {
        $.each(data, function (index, value) {
            // console.log(index);
            // console.log(value);
            if(index == 'homepage_kv') {
                $.each(value, function (i, detail) {
                    var html = '';
                    // console.log(detail.image_pc);
                    // console.log(detail.image_mb);
                    // console.log(detail.description);
                    // console.log(detail.href);
                    i = i+1;
                    html += '<div><div class="kv_text t'+i+'">'+detail.description+'</div>';
                    html += '<picture><source media="(max-width: 767px)" srcset="'+detail.image_mb+'">';
                    html += '<img src="'+detail.image_pc+'"></picture>';
                    html += '<a href="'+detail.href+'" class="more btn_hover">了解更多</a></div>';
                    $('.kv_slider').append(html);
                });
                // 首頁的KV輪播
                $('.kv_slider').slick({
                    dots: true,
                    infinite: true,
                    autoplay: true,
                    autoplaySpeed: 5000
                });
            } else if(index == 'homepage_topic') {
                $.each(value, function (i, detail) {
                    var html = '';
                    // console.log(detail.image_url);
                    // console.log(detail.topic);
                    // console.log(detail.href);
                    i = i+1;
                    html += '<div class="col-xs-12 col-sm-4"><img src="'+detail.image_url+'">';
                    html += '<div class="prod_box"><a href="'+detail.href+'">';
                    html += '<div class="prod-text prod-text--title">'+detail.topic+'</div></a></div>';
                    $('.homepage_topic .row').append(html);
                });
            } else if(index == 'homepage_year') {
                $.each(value, function (i, detail) {
                    var html = '';
                    // console.log(detail.year);
                    // console.log(detail.text);
                    i = i+1;
                    html += '<div><div class="green_circle"><p>'+detail.year;
                    html += '<span>'+detail.text+'</span></p></div></div>';
                    $('.year_slider').append(html);
                });

                // 大事記輪播
                $('.year_slider').slick({
                    dots: true,
                    infinite: true,
                    autoplay: false,
                    autoplaySpeed: 5000,
                    // centerMode: true,
                    // centerPadding: '50px',
                    slidesToShow: 1
                });
            } else if(index == 'homepage_store') {
                $.each(value, function (i, detail) {
                    var html = '';
                    // console.log(detail.image_url);
                    // console.log(detail.store);
                    // console.log(detail.address);
                    // console.log(detail.tel);
                    // console.log(detail.tag);
                    i = i+1;
                    html += '<div class="col-xs-12 col-sm-4"><img src="'+detail.image_url+'">';
                    html += '<div class="prod_box"><div class="prod-text prod-text--title">'+detail.store+'</div>';
                    html += '<div class="prod-text prod-text--center">'+detail.address+'</div>';
                    html += '<div class="prod-text prod-text--center">'+detail.tel+'</div>';
                    html += '<div class="prod-text prod-text--tag">'+detail.tag+'</div>';
                    html += '<div class="prod-text prod-text--center">'+detail.time+'</div></div></div>';
                    $('.homepage_store .row').append(html);
                });
            } else if(index == 'homepage_food') {
                $.each(value, function (i, detail) {
                    var html = '';
                    // console.log(detail.image_url);
                    // console.log(detail.store);
                    // console.log(detail.address);
                    // console.log(detail.tel);
                    // console.log(detail.tag);
                    i = i+1;
                    html += '<div class="col-xs-12 col-sm-6"><img src="'+detail.image_url_1+'">';
                    html += '<div class="prod_box"><div class="prod-text prod-text--title">'+detail.store+'</div>';
                    html += '<div class="prod-text prod-text--center">'+detail.address+'</div>';
                    html += '<div class="prod-text prod-text--center">'+detail.tel+'</div>';
                    html += '<div class="prod-text prod-text--tag">'+detail.tag+'</div></div></div>';
                    $('.homepage_food .row').append(html);
                });
            }
        });
    });

});

function readTextFile(file) {
    var rawFile = new XMLHttpRequest();
    rawFile.open("GET", file, false);
    rawFile.onreadystatechange = function() {
        if (rawFile.readyState === 4) {
            if (rawFile.status === 200 || rawFile.status == 0) {
                var allText = rawFile.responseText;
                console.log(allText);
                var kv_text_array = allText.split(',');
                for (var i = 1; i <= 3; i++) {
                    $('.kv_slider .t' + i).html(kv_text_array[i - 1]);
                }
            }
        }
    }
    rawFile.send(null);
}